<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Arsyfa Queen</title>    

    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    {!! JsonLd::generate() !!}
    {!! JsonLdMulti::generate() !!}

    <!-- OR -->
    {!! SEO::generate() !!}

    <!-- MINIFIED -->
    {!! SEO::generate(true) !!}

    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <script data-ad-client="ca-pub-9876588976105787" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-177833065-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-177833065-1');
    </script>

    <!-- <meta name="google-site-verification" content="" /> -->

    <!-- Favicon -->
    <!-- <link rel="icon" type="image/png" href=""> -->
</head>
<body>
    <style type="text/css">
        .text-white {
            color: white !important;
        }
        .main-theme-arsyfa {
            background-color: black !important;
            color: white;
        }
        .panel-tranparent {
            opacity: 0.9 !important;
            border-radius: 5px;
        }    
        .main-theme-arsyfa-button {
            border: solid 1px gray;
            padding: 5px;
            border-radius: 15px;
            background: black !important;
            color: white !important;
        }
    </style>
<header>
    <nav class="navbar navbar-default navbar-fixed-top main-theme-arsyfa">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#the-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand text-white" href="{{ route('blog') }}">Beranda</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="the-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="{{ route('blog') }}">Post</a></li>
                    <!-- <li><a href="#">About</a></li>
                    <li><a href="#">Contact</a></li> -->
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>
</header>

@yield('content')

<footer class="main-theme-arsyfa">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p class="copyright text-white">&copy; 2021 ArsyfaITSolution</p>
            </div>
            <div class="col-md-4">
                <nav>
                    <ul class="social-icons">
                        <li><a href="#" class="i fa fa-facebook text-white"></a></li>
                        <li><a href="#" class="i fa fa-twitter text-white"></a></li>
                        <li><a href="#" class="i fa fa-google-plus text-white"></a></li>
                        <li><a href="#" class="i fa fa-instagram text-white"></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</footer>

<script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>